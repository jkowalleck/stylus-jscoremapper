/* Sylus jsCoreMapper


 The MIT License (MIT)

 Copyright (c) 2014 Jan Kowalleck <jan.kowalleck@googlemail.com>

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

*/

module.exports = function (exposes) {
	var core = (function(){return this;}).call();

	var exposeVars = {};

	Object.keys(exposes).forEach(function(varname) {
		var coreVarname = exposes[ varname ];
		var coreVar = core[ coreVarname ];

		if ( typeof coreVar != 'undefined' )
		{
			exposeVars[ varname ] = coreVar;
		}
	});

	return function (style) {
		Object.keys(exposeVars).forEach(function(varname) {
			var variable = exposeVars[ varname ];
			switch ( typeof variable )
			{
				case 'object' :
					Object.getOwnPropertyNames(variable).forEach(function (prop)
					{
						style.define(varname+'-'+prop, variable[prop]);
					});
					break;
				default :
					style.define(varname, variable);
					break;
			}
		});



		this.evaluator.populateGlobalScope(); // take the newly defined vars into the evaluator
	};
};
