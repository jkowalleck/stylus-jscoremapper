
var path = require('path');
var fs = require('fs');

var testFile = path.join(__dirname, 'test.styl');
var stylus = require('stylus');

console.info(
	stylus.render(fs.readFileSync(testFile, 'UTF-8') ,
		{ compress : false
		, paths : [path.join(__dirname, '..')]
		})
);
